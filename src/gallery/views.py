from itertools import islice

from django.http.response import Http404
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response

from .forms import PhotoForm
from .models import Photo


def index(request):
    rows = []
    photos = Photo.objects.iterator()
    while True:
        row = tuple(islice(photos, 3))
        rows.append(row)
        if len(row) < 3:
            break

    return render_to_response(
        'index.html', {
            'rows': rows,
        }
    )


def edit_form(request, photo_id=None):
    'Возвращает форму редактирования фотографии.'
    if photo_id:
        try:
            photo = Photo.objects.get(id=photo_id)
        except Photo.DoesNotExist:
            raise Http404
    else:
        photo = None

    return render_to_response(
        'edit.html', {
            'request': request,
            'form': PhotoForm(instance=photo),
            'photo': photo,
        }
    )


def save_photo(request, photo_id=None):
    'Сохраняет изменения фотографии.'
    if photo_id:
        try:
            photo = Photo.objects.get(id=photo_id)
        except Photo.DoesNotExist:
            raise Http404
    else:
        photo = None

    form = PhotoForm(request.POST, instance=photo)
    if form.is_valid():
        form.save()

    return HttpResponseRedirect('/')
